<!--
% Linux遣い的Twitter FAQ
% Kenichiro MATOHARA
% 2016-03-30
-->

Q: スマホを持ってないけど使えるの?
A: PCのブラウザやTwitterアプリケーションで利用できます．勿論Linuxでも!

Q: Linuxで使うにはどうすればいいの?
A: ブラウザ(FirefoxやChromeがおすすめ)で[twitter](https://twitter.com)にアクセスしてアカウントを作成したら利用できるようになります．
便利に使うには[mikutter](http://mikutter.hachune.net/)（位置づけは[ジョークソフト](http://mikutter.blogspot.jp/2014/04/theday2014.html)）や[Tweetdeck](https://tweetdeck.twitter.com/)などのTwitter専用アプリケーションが便利です．

Q: 専用アプリはどういうふうに便利なの?
A: マルチカラム対応で特定の話題やグループ，リストなどを別々のカラムに表示して見やすく出来たり，ユーザストリームAPIに対応しているとリアルタイムに発言を見ることが可能です．mikutterにはプラグイン機能があって機能を拡張することも簡単に出来ます．Tweetdeckは同じアカウントを使うと他のブラウザでも同じ環境が復元されるので複数環境で利用する場合にも便利かもしれません．

Q: mikutterはどうやって導入するの?
A: メジャーな環境ではディストリビューションで[パッケージを用意されてている](http://yuzuki.hachune.net/wiki/MikutterInstallBattle)のでそういった環境では簡単に導入できます．

Q: Tweetdeckはどうやって導入するの?
A: [Tweetdeckのsite](https://tweetdeck.twitter.com/)でTwitterのID/Passwordでログインするだけです．

Q: 長いTweetが送れない
A: Twitterは1つのTweetにつき140文字までの制限があります．これを超えると送信できません．

Q: Twitter上の誰かに話しかけたい
A: 話しかけたいIDの前に @ を付けて話しかけると mention 機能で話しかけられます．例えば id:matoken に話しかけたい場合は「@matoken メッセージ」といった感じです．ちなみにこの内容は誰でも見ることが出来ます．

Q: 誰にも見られずに特定の人と会話したい
A: 話しかけたいIDの前に D をを付けて話しかけるとDM機能で話しかけられます．
例えば id:matoken に話しかけたい場合は「D matoken メッセージ」とします．
この時のメッセージの長さは通常のTweetと違い1万文字までです．ただ通知に出てこないので気づかれないことも多いです．

Q: ハッシュタグってなに?
A: 特定の話題を共有したい場合につける # から始まる文字列です．例えば鹿児島らぐのハッシュタグは [#kagolug](https://twitter.com/search?f=tweets&q=%23kagolug&src=typd)です．Twitter検索機能でブラウザやアプリケーションなどから絞り込みが出来ます．

Q: XXがわからない
A: TwitterやここやOFF会などで聞いてみてください．

